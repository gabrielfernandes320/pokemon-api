/*
    Lista apenas os pokemons até o número 152 da Pokedex Nacional
*/

var data = require("../data");

module.exports = function (req, res) {
  const kantoDex = data.filter((pokemon) => pokemon.national_number <= 152);
  res.json(kantoDex);
};
